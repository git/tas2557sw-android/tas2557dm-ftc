/*
** =============================================================================
** Copyright (c) 2016  Texas Instruments Inc.
**
** File:
**     tas2557_ftc.c
**
** Description:
**     factory test program for TAS2557 Android devices
**
** =============================================================================
*/

#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <sys/types.h>

#include "system.h"
#include "tas2557.h"        // TAS2557 Driver
#include "tas2557_ftc_lib.h"    
#include "tas2557_ftc.h"    // TAS2557 Factory Test and Calibration Tool

#define PI                   3.14159

// -----------------------------------------------------------------------------
// tas2557_ftc
// -----------------------------------------------------------------------------
// Description:
//      Obtains Re, f0, Q and T_cal from the speaker. This only needs to be
//      executed once during production line test.
// -----------------------------------------------------------------------------
int tas2557_chk(double t_cal, struct TFTCConfiguration *pFTCC)
{
	int nResult = 0;
	double nDevARe, nDevADeltaT, nDevAF0, nDevAQ;
	double nDevBRe, nDevBDeltaT, nDevBF0, nDevBQ;
	uint8_t nPGID;
	uint32_t libVersion;

	libVersion = get_lib_ver();
	printf("libVersion=0x%x\r\n", libVersion);

	/* get device PGID */
	tas2557_switch_device(pFTCC->nTSpkCharDevA.nDevAddr);
	nPGID = tas2557_get_PGID();
	printf("PGID=0x%x\r\n", nPGID);
	/* set device PGID to FTC process */
	tas2557_ftc_set_PGID(nPGID);

	/* Get actual Re from TAS2557 */
	tas2557_switch_device(pFTCC->nTSpkCharDevA.nDevAddr);
	nResult = get_Re_deltaT(pFTCC->nTSpkCharDevA.nPPC3_Re0,
							pFTCC->nTSpkCharDevA.nSpkReAlpha,
							&nDevARe, &nDevADeltaT);
	nResult = get_f0_Q(pFTCC->nTSpkCharDevA.nPPC3_FWarp,
						pFTCC->nPPC3_FS,
						pFTCC->nTSpkCharDevA.nPPC3_Bl,
						pFTCC->nTSpkCharDevA.nPPC3_Mms,
						pFTCC->nTSpkCharDevA.nPPC3_Re0,
						&nDevAF0, &nDevAQ);
	printf("SPK_A Re = %f, DeltaT=%f, F0 = %f, Q = %f\n", nDevARe, nDevADeltaT, nDevAF0, nDevAQ);

	tas2557_switch_device(pFTCC->nTSpkCharDevB.nDevAddr);
	nResult = get_Re_deltaT(pFTCC->nTSpkCharDevB.nPPC3_Re0,
							pFTCC->nTSpkCharDevB.nSpkReAlpha,
							&nDevBRe, &nDevBDeltaT);
	nResult = get_f0_Q(pFTCC->nTSpkCharDevB.nPPC3_FWarp,
						pFTCC->nPPC3_FS,
						pFTCC->nTSpkCharDevB.nPPC3_Bl,
						pFTCC->nTSpkCharDevB.nPPC3_Mms,
						pFTCC->nTSpkCharDevB.nPPC3_Re0,
						&nDevBF0, &nDevBQ);
	printf("SPK_B Re = %f, DeltaT=%f, F0 = %f, Q = %f\n", nDevBRe, nDevBDeltaT, nDevBF0, nDevBQ);

	tas2557_ftc_release();

    return nResult;
}
