/*
** =============================================================================
** Copyright (c) 2016  Texas Instruments Inc.
**
** File:
**     tas2557_ftc.c
**
** Description:
**     factory test program for TAS2557 Android devices
**
** =============================================================================
*/

#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <sys/types.h>

#include "system.h"
#include "tas2557.h"        // TAS2557 Driver
#include "tas2557_ftc_lib.h"    
#include "tas2557_ftc.h"    // TAS2557 Factory Test and Calibration Tool

#define PI                   3.14159

static struct TFTCConfiguration *gpFTCC;

// -----------------------------------------------------------------------------
// tas2557_ftc
// -----------------------------------------------------------------------------
// Description:
//      Obtains Re, f0, Q and T_cal from the speaker. This only needs to be
//      executed once during production line test.
// -----------------------------------------------------------------------------
uint32_t tas2557_ftc(double t_cal, struct TFTCConfiguration *pFTCC)
{
	int nResult = 0;
	double dev_a_re = pFTCC->nTSpkCharDevA.nPPC3_Re0;   // Default Re
	uint32_t dev_a_prm_pow = 0;           // Total RMS power coefficient
	uint32_t dev_a_prm_tlimit = 0;        // Delta temperature limit coefficient
	double dev_b_re = pFTCC->nTSpkCharDevB.nPPC3_Re0;   // Default Re
	uint32_t dev_b_prm_pow = 0;           // Total RMS power coefficient
	uint32_t dev_b_prm_tlimit = 0;        // Delta temperature limit coefficient
	uint8_t nPGID;
	uint32_t libVersion;
	uint32_t result = 0;
	pid_t nPlaybackProcess;

	gpFTCC = pFTCC;

	libVersion = get_lib_ver();
	printf("libVersion=0x%x\r\n", libVersion);

	/* get device PGID */
	tas2557_switch_device(gpFTCC->nTSpkCharDevA.nDevAddr);
	nPGID = tas2557_get_PGID();
	printf("PGID=0x%x\r\n", nPGID);

	/* set device PGID to FTC process */
	tas2557_ftc_set_PGID(nPGID);

	// STEP 1: Play calibration signal
	tas2557_mixer_command("PRI_MI2S_RX Audio Mixer MultiMedia1", 1); //platform dependent
	nPlaybackProcess = sys_play_wav("silense.wav", "loop");

	// STEP 2: start calibration process
	tas2557_switch_device(gpFTCC->nTSpkCharDevA.nDevAddr);
    tas2557_ftc_start();
	tas2557_switch_device(gpFTCC->nTSpkCharDevB.nDevAddr);
	tas2557_ftc_start();

	// STEP 3: Wait for algorithm to converge
	sys_delay(gpFTCC->nCalibrationTime); // Delay 

	// STEP 4: Get actual Re from TAS2557
	tas2557_switch_device(gpFTCC->nTSpkCharDevA.nDevAddr);
	dev_a_re = get_re(gpFTCC->nTSpkCharDevA.nPPC3_Re0);
	tas2557_switch_device(gpFTCC->nTSpkCharDevB.nDevAddr);
	dev_b_re = get_re(gpFTCC->nTSpkCharDevB.nPPC3_Re0);
	printf("dev_a Re=%f, dev_b Re=%f\r\n", dev_a_re, dev_b_re);

	// STEP 5: check speaker bounds
	nResult = check_spk_bounds(gpFTCC, dev_a_re, dev_b_re);

	// STEP 6: Set temperature limit to target TMAX
	if ((nResult& RE1_CHK_MSK) == RESULT_PASS) {
		tas2557_switch_device(gpFTCC->nTSpkCharDevA.nDevAddr);
		dev_a_prm_pow = calc_prm_pow (dev_a_re, 
			gpFTCC->nTSpkCharDevA.nSpkTMax - t_cal, 
			gpFTCC->nTSpkCharDevA.nPPC3_RTV, 
			gpFTCC->nTSpkCharDevA.nPPC3_RTM, 
			gpFTCC->nTSpkCharDevA.nPPC3_RTVA, 
			gpFTCC->nTSpkCharDevA.nPPC3_SysGain);
		dev_a_prm_tlimit = calc_prm_tlimit(gpFTCC->nTSpkCharDevA.nSpkTMax - t_cal, 
			gpFTCC->nTSpkCharDevA.nSpkReAlpha, 
			gpFTCC->nTSpkCharDevA.nPPC3_DevNonlinPer, 
			gpFTCC->nTSpkCharDevA.nPPC3_RTV, 
			gpFTCC->nTSpkCharDevA.nPPC3_RTM, 
			gpFTCC->nTSpkCharDevA.nPPC3_RTVA,
			gpFTCC->nTSpkCharDevA.nPPC3_PIG);
		set_re(gpFTCC->nTSpkCharDevA.nPPC3_Re0, dev_a_re, gpFTCC->nTSpkCharDevA.nSpkReAlpha);
		set_temp_cal(dev_a_prm_pow, dev_a_prm_tlimit);
	}

 	if ((nResult& RE2_CHK_MSK) == RESULT_PASS) {
		tas2557_switch_device(gpFTCC->nTSpkCharDevB.nDevAddr);
		dev_b_prm_pow = calc_prm_pow (dev_b_re, 
			gpFTCC->nTSpkCharDevB.nSpkTMax - t_cal, 
			gpFTCC->nTSpkCharDevB.nPPC3_RTV, 
			gpFTCC->nTSpkCharDevB.nPPC3_RTM, 
			gpFTCC->nTSpkCharDevB.nPPC3_RTVA, 
			gpFTCC->nTSpkCharDevB.nPPC3_SysGain);
		dev_b_prm_tlimit = calc_prm_tlimit(gpFTCC->nTSpkCharDevB.nSpkTMax - t_cal, 
			gpFTCC->nTSpkCharDevB.nSpkReAlpha, 
			gpFTCC->nTSpkCharDevB.nPPC3_DevNonlinPer, 
			gpFTCC->nTSpkCharDevB.nPPC3_RTV, 
			gpFTCC->nTSpkCharDevB.nPPC3_RTM, 
			gpFTCC->nTSpkCharDevB.nPPC3_RTVA,
			gpFTCC->nTSpkCharDevB.nPPC3_PIG);
		set_re(gpFTCC->nTSpkCharDevB.nPPC3_Re0, dev_b_re, gpFTCC->nTSpkCharDevB.nSpkReAlpha);
		set_temp_cal(dev_b_prm_pow, dev_b_prm_tlimit);
	}

	// STEP 7: start calibration process
	tas2557_switch_device(gpFTCC->nTSpkCharDevA.nDevAddr);
	tas2557_ftc_stop();
	tas2557_switch_device(gpFTCC->nTSpkCharDevB.nDevAddr);
	tas2557_ftc_stop();

	sys_stop_wav(nPlaybackProcess);

	// STEP 8: Save Re, and Cal Temp into a file
	tas2557_save_cal(gpFTCC, dev_a_re, dev_a_prm_pow, dev_a_prm_tlimit, 
		dev_b_re, dev_b_prm_pow, dev_b_prm_tlimit, 
		t_cal, nResult, "tas2557_cal.txt");

    // STEP 9: Save .bin file for TAS2555 driver
	if ((nResult & (RE1_CHK_MSK | RE2_CHK_MSK)) == RESULT_PASS) {
		tas2557_open_bin("tas2557_cal.bin", gpFTCC->nTSpkCharDevA.nDevAddr, gpFTCC->nTSpkCharDevB.nDevAddr);
		if ((nResult & RE1_CHK_MSK) == RESULT_PASS) {
			tas2557_switch_device(gpFTCC->nTSpkCharDevA.nDevAddr);
			set_re(gpFTCC->nTSpkCharDevA.nPPC3_Re0, dev_a_re, gpFTCC->nTSpkCharDevA.nSpkReAlpha);
			set_temp_cal(dev_a_prm_pow, dev_a_prm_tlimit);
		}

		if ((nResult & RE2_CHK_MSK) == RESULT_PASS) {
			tas2557_switch_device(gpFTCC->nTSpkCharDevB.nDevAddr);
			set_re(gpFTCC->nTSpkCharDevB.nPPC3_Re0, dev_b_re, gpFTCC->nTSpkCharDevB.nSpkReAlpha);
			set_temp_cal(dev_b_prm_pow, dev_b_prm_tlimit);
		}

		tas2557_close_bin();
	}

	if (gpFTCC->bLoadCalibration)
		tas2557_load_calibration(0xFF);

	tas2557_ftc_release();

    return result;
}
