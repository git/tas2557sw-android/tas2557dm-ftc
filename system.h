/*
** =============================================================================
** Copyright (c) 2016  Texas Instruments Inc.
**
** File:
**     system.h
**
** Description:
**     header file for system.c
**
** =============================================================================
*/

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include <stdint.h>
#include <sys/types.h>

#define AUDIO_PLAYER "/system/bin/tinyplay"
#define AUDIO_MIXER "/system/bin/tinymix"

// Hypothetical System Functions
extern pid_t sys_play_wav(char * pFile, char * pMode);
extern void sys_stop_wav(pid_t nProcess);
extern void sys_delay(uint32_t delay_ms);
extern double sys_get_ambient_temp(void);
extern int sys_is_valid(char * pFile);

#endif /* SYSTEM_H_ */
