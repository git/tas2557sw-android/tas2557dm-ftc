/*
** =============================================================================
** Copyright (c) 2016  Texas Instruments Inc.
**
** File:
**     tas2557_lib_ftc.h
**
** Description:
**     header file for tas2557_lib_ftc.c
**
** =============================================================================
*/

#ifndef TAS2557_LIB_FTC_H_
#define TAS2557_LIB_FTC_H_

#include <stdint.h>
#include <stdbool.h>

#include "tas2557.h"

int get_lib_ver(void);
void tas2557_ftc_set_PGID(unsigned char nPGID);
void tas2557_ftc_start(void);
void set_re(double re_ppc3, double re, double alpha);
void set_temp_cal(uint32_t prm_pow, uint32_t prm_tlimit);
double get_re(double re_ppc3);
double CalcRe(double re_ppc3, uint32_t prm_r0);
uint32_t calc_prm_pow(double re, double delta_t_max, double nRTV, double nRTM, double nRTVA, double nSysGain);
uint32_t calc_prm_tlimit(double delta_t_max, double alpha, double nDevNonlinPer, double nRTV, double nRTM, double nRTVA, double nPIG);
void tas2557_ftc_stop(void);

/* below functions are used in SPK measurement only */
int get_Re_deltaT(double nPPC3_Re, double nPPC3_alpha, double *pnRe, double *pnDeltaT);
int get_f0_Q(double nPPC3_FWarp, double nPPC3_nFS, double nPPC3_Bl, double nPPC3_Mms, double nPPC3_Re0, double *pnF0, double *pnQ);

#endif /* TAS2557_LIB_FTC_H_ */
